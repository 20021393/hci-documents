# HCI-Documents

This is the description of a job management program in which one job may depend on many other jobs and have completion requirements.

## Data Structure Design

A **job** contains the following fields:
 - ID
 - Name
 - Description
 - List of requirements
 - Completion status
 - List of jobs that it depends on and how it depends on them
Job data can be entered by the user or input via an API. The system stores the source of input data.
The following operations are supported:
 - Connect job
 - Modify job
    1. ID cannot be changed.
    2. Name can be changed.
    3. Description can be modified
    4. Requirements can be added, removed or modified.
    5. Completion status can be toggled.
    6. List of dependencies can be modified.
A **project** or **notebook** is a *collection* of interrelated jobs connected as a job. Projects can be connected to each other as equals or one project can be incorporated recursively as a job of another project.
When the user opens a job, the most important dependencies are displayed. Importance can be calculated based on self-assigned numerical weight, number of jobs that depend on it, deadline proximity and other factors. The program also suggests the most efficient way to complete the task.
An edit history is used to store actions by the user so any operation can be efficiently undone.
The following principles have been used in the design of the software:
 - Forgiveness: Any action can be easily undone.
 - Consistency: There is only one data type: job. A project is a collection of jobs that is itself a job and encodes its members as dependencies.
 - Error handling: The error model is simple: A job cannot be completed before its dependencies are completed. 
 - Shortcuts: Even a set of bookmarks is stored as a job.
 - Informative feedback: when a job is blocked, its blocking dependencies are shown recursively up to certain base dependencies defined by the user.